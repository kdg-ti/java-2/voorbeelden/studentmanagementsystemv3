package be.kdg.java2.sms.view.implementation;

import be.kdg.java2.sms.view.LoginView;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class LoginViewImpl extends BorderPane implements LoginView {
    private static final Logger L = Logger.getLogger(LoginViewImpl.class.getName());
    private final Button btnAdd;
    private final Button btnLogin;
    private final TextField tfName;
    private final PasswordField pfPassword;

    public LoginViewImpl() {
        L.info("Constructing LoginView!");
        btnAdd = new Button("Add");
        btnLogin = new Button("Login");
        tfName = new TextField();
        pfPassword = new PasswordField();
        GridPane gp = new GridPane();
        gp.add(new Label("User:"), 0,0);
        gp.add(tfName, 1,0);
        gp.add(new Label("Password:"), 0,1);
        gp.add(pfPassword, 1,1);
        gp.setHgap(5);
        gp.setVgap(5);
        BorderPane.setMargin(gp,new Insets(10));
        this.setCenter(gp);
        HBox hBox = new HBox(btnLogin, btnAdd);
        hBox.setSpacing(10);
        BorderPane.setMargin(hBox,new Insets(10));
        this.setBottom(hBox);
    }

    @Override
    public Button getBtnAdd() {
        return btnAdd;
    }

    @Override
    public Button getBtnLogin() {
        return btnLogin;
    }

    @Override
    public TextField getTfName() {
        return tfName;
    }

    @Override
    public PasswordField getPfPassword() {
        return pfPassword;
    }
}

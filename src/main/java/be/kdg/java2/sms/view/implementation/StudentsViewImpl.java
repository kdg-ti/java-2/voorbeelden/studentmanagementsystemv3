package be.kdg.java2.sms.view.implementation;

import be.kdg.java2.sms.service.models.Student;
import be.kdg.java2.sms.view.StudentsView;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class StudentsViewImpl extends BorderPane implements StudentsView {
    private final TableView<Student> tvStudents;
    private final TextField tfName;
    private final DatePicker dpBirth;
    private final TextField tfLength;
    private final Button btnSave;

    public StudentsViewImpl() {
        tvStudents = new TableView<>();
        TableColumn<Student, String> column1 = new TableColumn<>("Name");
        column1.setCellValueFactory(new PropertyValueFactory<>("name"));
        TableColumn<Student, LocalDate> column2 = new TableColumn<>("Birthday");
        column2.setCellValueFactory(new PropertyValueFactory<>("birthday"));
        TableColumn<Student, String> column3 = new TableColumn<>("Length");
        column3.setCellValueFactory(new PropertyValueFactory<>("length"));
        tvStudents.getColumns().add(column1);
        tvStudents.getColumns().add(column2);
        tvStudents.getColumns().add(column3);
        setCenter(tvStudents);
        BorderPane.setMargin(tvStudents, new Insets(10));
        tfName = new TextField();
        tfName.setPromptText("Naam");
        btnSave = new Button("Save");
        btnSave.setMinWidth(Button.USE_PREF_SIZE);
        dpBirth = new DatePicker();
        dpBirth.setPromptText("Geboorte");
        tfLength = new TextField();
        tfLength.setPromptText("Length");
        HBox hbox = new HBox(tfName, dpBirth, tfLength, btnSave);
        BorderPane.setMargin(hbox, new Insets(10));
        hbox.setSpacing(10);
        this.setBottom(hbox);
    }

    @Override
    public TableView<Student> getTvStudents() {
        return tvStudents;
    }

    @Override
    public TextField getTfName() {
        return tfName;
    }

    @Override
    public DatePicker getDpBirth() {
        return dpBirth;
    }

    @Override
    public TextField getTfLength() {
        return tfLength;
    }

    @Override
    public Button getBtnSave() {
        return btnSave;
    }
}

package be.kdg.java2.sms.service.implementation;

import be.kdg.java2.sms.database.StudentDAO;
import be.kdg.java2.sms.service.models.Student;
import be.kdg.java2.sms.service.StudentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentsServiceImpl implements StudentsService {
    private StudentDAO studentDAO;

    @Autowired
    public StudentsServiceImpl(StudentDAO studentDAO) {
        this.studentDAO = studentDAO;
    }

    @Override
    public List<Student> getAllStudents(){
        return studentDAO.retreiveAll();
    }

    @Override
    public void addStudent(Student student) {
        studentDAO.add(student);
    }
}

package be.kdg.java2.sms.service.implementation;

import be.kdg.java2.sms.database.UserDAO;
import be.kdg.java2.sms.exceptions.StudentException;
import be.kdg.java2.sms.service.models.User;
import be.kdg.java2.sms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger L = Logger.getLogger(UserServiceImpl.class.getName());
    private static final int MIN_PASSWORD_LENGTH = 3;//todo: change to 10!
    private UserDAO userDAO;

    @Autowired
    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public void addUser(String name, String password) throws StudentException {
        L.info("Trying to add user:" + name);
        if (password==null||password.length()<MIN_PASSWORD_LENGTH) {
            L.info("Password not strong enough");
            throw new StudentException("Password not strong enough");
        }
        if (userDAO.getUserByName(name)!=null) {
            L.info("Username already in use!");
            throw new StudentException("Username already in use!");
        }
        userDAO.addUser(new User(name, password));
    }

    @Override
    public boolean login(String username, String password) throws StudentException {
        L.info("Trying to login user " + username);
        User user = userDAO.getUserByName(username);
        if (user == null) {
            return false;
        } else {
            return user.getPassword() != null && user.getPassword().equals(password);
        }
    }
}

package be.kdg.java2.sms.database.hsql;

import be.kdg.java2.sms.database.DataSource;
import be.kdg.java2.sms.database.UserDAO;
import be.kdg.java2.sms.exceptions.StudentException;
import be.kdg.java2.sms.service.models.User;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

@Repository
public class HSQLUserDAO implements UserDAO {
    private static final Logger L = Logger.getLogger(HSQLUserDAO.class.getName());

    @Override
    public User getUserByName(String name) {
        L.info("Getting user by name " + name);
        try {
            PreparedStatement preparedStatement = DataSource.getInstance().getConnection().prepareStatement("SELECT * FROM USERS WHERE NAME = ?");
            preparedStatement.setString(1,name);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return new User(rs.getString("NAME"), rs.getString("PASSWORD"));
            } else {
                return null;
            }
        } catch (SQLException e) {
            L.warning("Problem while getting the user:" + e.getMessage());
            throw new StudentException(e);
        }
    }

    @Override
    public void addUser(User user) {
        try {
            PreparedStatement preparedStatement = DataSource.getInstance().getConnection().prepareStatement("INSERT INTO USERS VALUES (NULL, ?, ?)");
            preparedStatement.setString(1,user.getName());
            preparedStatement.setString(2,user.getPassword());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            L.warning("Problem while adding the user:" + e.getMessage());
            throw new StudentException(e);
        }
    }
}
